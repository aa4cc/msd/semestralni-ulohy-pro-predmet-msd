%% This script serves for visualization of datasets from the System: Array of Pendulums.

load('Datasets_array_7_pendulums_MSD.mat'); % Loading the datasets
num_dataset = numel(datasets);

% ############################################################
% ##### Displaying a dataset based on user prompt #####
% clear prompt;
% prompt = "### Input a dataset index to visualize (integer value from 1 to " + num2str(num_dataset) + "): ";
% data_idx = input(prompt);
% 
% if (data_idx < 1 || data_idx > num_dataset || mod(data_idx,1) ~= 0)
%     error("Input is out of range or is not an integer");
% end


% ############################################################
% ##### Displaying all datasets #####
% ## Note: do not forger to un/comment the keyword "end" at the end of the script 

for ii = 1:num_dataset
    data_idx = ii;


% ############################################################
% ##### Displaying only one dataset ##### 
% data_idx = 1;



figure('Position',[100 100, 800, 600]);

time = datasets{data_idx}.time;
angles = datasets{data_idx}.angles;
in_torque = datasets{data_idx}.input;

subplot(2,1,1);
hold on;
title(datasets{data_idx}.desc);
plot(time, angles, 'LineWidth',1.5);
xlabel("Time [s]");
ylabel("Angle [rad]");
num_pend = size(angles, 2);
xlim([0, time(end)]);

legend_text = num2cell(sprintf('%d', 1:1:num_pend));
legend(legend_text);
grid on;
box on;

subplot(2,1,2);
plot(time, in_torque, 'LineWidth',1.5);
xlabel("Time [s]");
ylabel("Torque [N m]");
grid on;
box on;
xlim([0, time(end)]);


end




