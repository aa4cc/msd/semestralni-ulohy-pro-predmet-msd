a=input('Input no. of experiment ');

switch a
    case 1
        load('dataset1.mat');    
        s = simout1;
        fprintf('Dataset n.1: Pump step response.\n')
    
    case 2
        load('dataset2.mat');
        s = simout2;        
        fprintf('Dataset n.2: Maximal pump load.\n')
        
    case 3
        load('dataset3.mat');
        s = simout3;
        fprintf('Dataset n.3. for identification/verification.\n');
        fprintf('t in <0, 18.5)-> Pump 0, V1 close, V2 close \n')
        fprintf('t in <18.5, 33.5)->Pump 100, V1 close,  V2 close \n')
        fprintf('t in <33.5, 200) -> Pump 36, V1 close,  V2 open \n')
        fprintf('t in <200, 400)  -> Pump 36, V1 open,  V2 close \n')
        fprintf('t in <400, 550)  -> Pump 36, V1 open,  V2 open \n')
        fprintf('t in <550, 650)  -> Pump 0,  V1 open,  V2 open \n')
        fprintf('t in <650, end)  -> Pump 0,  V1 close, V2 close \n')
    case 4
        load('dataset4.mat');
        s = verif1;
        fprintf('Dataset n.4. for identification/verification.\n');
    otherwise
        warning('Wrong number of experiment!')
        
end

time = s.time;
% data = ;
figure;
s.signals.values(:,4) = not(s.signals.values(:,4));
plot(time,[s.signals.values(:,3:5) s.signals.values(:,8:11)], 'linewidth',1);
hold on;
s.signals.values(:,14) = not(s.signals.values(:,14) + 1);
s.signals.values(:,15) = not((s.signals.values(:,15)) + 1);
plot(time,s.signals.values(:,13:15), 'linestyle','--');
legend('Set Pump Load [0-100%]', 'Set V1 \{0,1\}','Set V2 [0-100%]','Actual Pump Load [0-100%]', ...
    'Left tank (N1) Water Level [cm]','Right tank (N2) Water Level [cm]','Lower Tank (N3) Water Level [cm]', ...
    'V2 position [0-100%]','V1 moving [-]','V1 open \{0,1\}')
axis([0 max(time) -10 120])
title('Most important Water plant signals')

